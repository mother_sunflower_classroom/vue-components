export default [
  {
    name: '横向表格',
    router: 'horGrid'
  },
  {
    name: 'box框',
    router:'box'
  },
  {
    name: '复制input内容按钮',
    router:'copy'
  },
  {
    name: '下拉选择框，树',
    router: 'selectTree'
  },
  {
    name: 'elementUI表格',
    router: 'grid'
  },
  {
    name: '文字滚动',
    router: 'scrollText'
  },
  {
    name: '创建窗口',
    router: 'infoWindow'
  },
  {
    name: '数独',
    router: 'number'
  }
]
