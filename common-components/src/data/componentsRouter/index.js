import homePage from '../../homePage'

export default [{
  path: '/',
  name: 'home',
  component: homePage,
  children: [
    {
      path: '/horGrid',
      name: 'horGrid',
      component: () =>
        import('../../components/horGrid')
    },
    {
      path: '/box',
      name: 'box',
      component: () =>
        import('../../components/box')
    },
    {
      path: '/copy',
      name: 'copy',
      component: () =>
        import('../../components/copy')
    },
    {
      path: '/selectTree',
      name: 'selectTree',
      component: () =>
        import('../../components/selectTree')
    },
    {
      path: '/grid',
      name: 'grid',
      component: () =>
        import('../../components/grid')
    },
    {
      path: '/scrollText',
      name: 'scrollText',
      component: () =>
        import('../../components/scrollText')
    },
    {
      path: '/infoWindow',
      name: 'infoWindow',
      component: () =>
        import('../../components/infoWindow')
    },
    {
      path: '/number',
      name: 'number',
      component: () =>
        import('../../components/number')
    }
  ]
}]
