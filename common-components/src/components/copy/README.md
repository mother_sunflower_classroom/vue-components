此组件主要是记录怎样通过按钮复制内容

需要引入clipboard模块 

clipboard官网： https://clipboardjs.com/

npm install clipboard -s

import clipboard from 'clipboard';
//注册到vue原型上
Vue.prototype.clipboard = clipboard;

具体样式可以在组件上修改
