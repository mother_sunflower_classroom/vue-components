这个表格是根据elementUI表格table进一步封装的，

使用时一定要添加column和page参数，为必填项

column:  [
  {
    lable： 表头名称
    prop： 本表头所对应data的key值
  }
]
page: {
  pageSizes: [20, 50, 100],  选择本页显示多少个
  total: 1, 总共有多少个数据
  currentPage: 1,  当前页数
  pageSize:20  每页显示多少数据
}
rowKey： 当前表格每一行的对应id ， 不填时data中必须有id字段，或者自己定义字段
showBtn： 是否显示操作栏

showIndex： 是否显示勾选

current-change  当前页变化触发的事件

size-change： 页数变化触发的事件
