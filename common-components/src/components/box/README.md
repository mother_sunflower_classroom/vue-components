这是一个box组件，
showTitle 属性         控制是否显示表题栏
overFlowShow 属性      默认content区域是上下滚动的，设置为'overflow: hidden;'值，上下不滚动
leftTitle 属性         左侧表题的内容
rightTitle 属性        右侧表题的内容
titleBg 属性           表题的背景色
contentBg 属性         内容区域的背景色
titleFontColor 属性    表头的文字颜色

rightClick 事件        点击右侧表题触发事件

通过<template solt="content">html</template>设置内容
