这个组件是下拉框为树形结构


可以传3个参数 
    option： {
      label： 下拉框的label名称
      labelWidth：  label的宽度
      placeholder： 没有选中时的内容
      defaultId： 默认选中的节点
    }

    expandAry： 是一个数组，为默认展开的树节点id
    data： 数据
